package Model;

import java.util.ArrayList;

public class Player {

    private ArrayList<Card> playerDeck;
    private String username;

    private int numberOfTurns;

    public Player (String username) {
        playerDeck = new ArrayList<>();
        this.username = username;
        numberOfTurns = 0;
    }

    public ArrayList<Card> getPlayerDeck() {
        return playerDeck;
    }

    public void setPlayerDeck(ArrayList<Card> playerDeck) {
        this.playerDeck = playerDeck;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Card playCard(int index) {
        return playerDeck.remove(index);
    }

    public boolean playCard(Card card) {
        return playerDeck.remove(card);
    }

    public void addCard(Card card) {
        playerDeck.add(card);
    }

    public String displayDeck () {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < playerDeck.size(); i++) {
            string.append(playerDeck.get(i));
            if (i<playerDeck.size()-1) {
                string.append("~");
            }
        }
        return string.toString();
    }

    public int getNumberOfTurns() {
        return numberOfTurns;
    }

    public int getNumberOfSameCard (Card card) {
        int count = 0;
        for (Card card1: playerDeck) {
            if (card1.equals(card)){
                count++;
            }
        }
        return count;
    }

    public void setNumberOfTurns(int numberOfTurns) {
        this.numberOfTurns = numberOfTurns;
    }

    public boolean canNope() {
        return playerDeck.contains(Card.NOPE);
    }
}
