package Server;

import Model.Card;
import Model.Player;
import Server.Controller.ClientHandler;
import Server.Controller.Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ExplodingKittensServer {

    private static int PORT = 12345;
    public static List<ClientHandler> clients = new ArrayList<>();

    public static Game game = new Game();

    public static int numberOfPlayers = 0;

    public static boolean atStart = false;

    public static void main(String[] args) {
        new ExplodingKittensServer().startServer();
    }

    public void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server is running and waiting for connections...");

            while (true) {
                if (numberOfPlayers<2) {
                    Socket clientSocket = serverSocket.accept();
                    System.out.println("New connection: " + clientSocket);

                    ClientHandler clientHandler = new ClientHandler(clientSocket, this);
                    clients.add(clientHandler);
                    new Thread(clientHandler).start();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void broadcastMessage(String message, ClientHandler sender) {
        for (ClientHandler client : clients) {
            if (client != sender) {
                client.sendMessage(message);
            }
        }
    }

    public void broadcastMessageWithSender(String message) {
        for (ClientHandler client : clients) {
            client.sendMessage(message);
        }
    }

    public void removeClient(ClientHandler clientHandler) {
        clients.remove(clientHandler);
    }



}
