package Server.Controller;

import Model.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Game {

    private ArrayList<Card> gameDeck = new ArrayList<>();

    public Game() {
        for (int i = 0; i < 5; i++) {
            if (i<4) {
                gameDeck.add(Card.ATTACK);
                gameDeck.add(Card.FAVOR);
                gameDeck.add(Card.SHUFFLE);
                gameDeck.add(Card.SKIP);
                gameDeck.add(Card.CAT1);
                gameDeck.add(Card.CAT2);
                gameDeck.add(Card.CAT3);
                gameDeck.add(Card.CAT4);
                gameDeck.add(Card.CAT5);
            }
            gameDeck.add(Card.NOPE);
            gameDeck.add(Card.FUTURE);

            shuffle();
        }
    }

    public void shuffle() {
        Collections.shuffle(gameDeck);
    }

    public Card drawCard () {
        return gameDeck.remove(0);
    }

    public void addExplodingKitten () {
        Random random = new Random();
        gameDeck.add(random.nextInt(1,gameDeck.size()),Card.EXPLODING_KITTEN);
    }

    public void addExplodingKitten (int index) {
        gameDeck.add(index,Card.EXPLODING_KITTEN);
    }

    public void addDefuseCard () {
        Random random = new Random();
        gameDeck.add(random.nextInt(gameDeck.size()),Card.DEFUSE);
    }

    public ArrayList<Card> getPlayerDeck () {
        ArrayList<Card> playerDeck = new ArrayList<>();
        playerDeck.add(Card.DEFUSE);
        for (int i = 0; i < 7; i++) {
            playerDeck.add(drawCard());
        }
        shuffle();
        return playerDeck;
    }

    public ArrayList<Card> getGameDeck() {
        return gameDeck;
    }



}
