package Server.Controller;

import Model.Card;
import Model.Player;
import Server.ExplodingKittensServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

import static Server.ExplodingKittensServer.*;

public class ClientHandler implements Runnable {

    /**
     * @invariant socket and server references should not be changed after constructing the object
     * @invariant writer object should also be the same once class is instanced
     */
    private Socket socket;
    private ExplodingKittensServer server;
    private PrintWriter writer;
    public Player player;

    public static Card cardToBeNope;


    /**
     *
     * @param socket used to connect client to server
     * @param server used to host the game
     * @ensures proper connection for class variables following socket programming
     * @requires socket and server not null
     */
    public ClientHandler(Socket socket, ExplodingKittensServer server) {
        this.socket = socket;
        this.server = server;
        try {
            this.writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * @ensures handling the messages from clients through sendMessage method using a reader in the socket
     * @ensures removing and disconnecting the client from the list of clients once disconnected
     */
    @Override
    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            String message;

            while ((message = reader.readLine()) != null) {

                // Handle the received message, implement game logic here
                sendMessage(handleCommand(message));

                if (ExplodingKittensServer.atStart) {
                    ExplodingKittensServer.atStart = false;
                }

                if (player!=null) {
                    server.broadcastMessage(player.getUsername() + ": " + message, this);
                }
            }
        } catch (IOException e) {
            // Handle disconnection or errors
            e.printStackTrace();
        } finally {
            // Cleanup and remove the client from the list
            server.removeClient(this);
            System.out.println("Client disconnected: " + socket);
        }
    }

    /**
     *
     * @param message to be sent to the server
     * @requires message to be following communication protocol
     * @ensures message is sent to the server to be processed
     */
    public void sendMessage(String message) {
        if (!message.equalsIgnoreCase("")) {
            writer.println(message);
        }
    }


    /**
     *
     * @param command from client to be handled by the server
     * @requires command to be a non-null string, adhering to the protocol defined by the mentoring group
     * @requires a server class to be running
     * @ensures proper handling of the command sent by the client to server, as well as updating the game model
     * @ensures proper updates for the game model, handling turn counting
     * @return string indicating current response from the client to the server, adhering to communication protocol
     */
    public String handleCommand (String command) {
        if (command.equalsIgnoreCase("HI~COMBOS")) {
            return "HI~COMBOS";
        } else if (command.startsWith("CONNECT")) {
             player = new Player(command.split("~")[1]);
             player.setPlayerDeck(ExplodingKittensServer.game.getPlayerDeck());
             return "CONNECTED";
        } else if (command.startsWith("REQUEST_GAME")) {
            if (ExplodingKittensServer.numberOfPlayers==1) {
                server.broadcastMessageWithSender("GAME_STARTED~COMBOS");
                game.addExplodingKitten();
                game.addDefuseCard();
                game.addDefuseCard();
                game.shuffle();
                System.out.println("Game Deck: ");
                for (int i = 0; i < ExplodingKittensServer.game.getGameDeck().size(); i++) {
                    System.out.println("Card " + (i+1) + ": " + ExplodingKittensServer.game.getGameDeck().get(i) );
                }
                Random random = new Random();
                int turn = random.nextInt(2);
                clients.get(turn).player.setNumberOfTurns(1);
                sendMessage("RESPONSE_CARDS_IN_HAND~" + player.displayDeck());
                server.broadcastMessageWithSender("TURN~" + clients.get(turn).player.getUsername());
                ExplodingKittensServer.atStart = true;
                ExplodingKittensServer.numberOfPlayers = 0;
            } else {
                ExplodingKittensServer.numberOfPlayers++;
            }
        } else if (command.startsWith("REQUEST_CARDS_IN_HAND")){
            return "RESPONSE_CARDS_IN_HAND~" + player.displayDeck();
        } else if (command.startsWith("DRAW_CARD")) {
            server.broadcastMessage("ANNOUNCEMENT~" + player.getUsername() + "~DRAWN",this);
            Card card = game.drawCard();
            if (card.equals(Card.EXPLODING_KITTEN)) {
                server.broadcastMessageWithSender("ANNOUNCEMENT~" + player.getUsername() + "~DRAWN_EXPLODING_KITTEN");
                if (player.getPlayerDeck().contains(Card.DEFUSE)) {
                    return "PLAY_DEFUSE";
                } else {
                    server.broadcastMessageWithSender("GAME_FINISHED~" + clients.get(getOtherClient()).player.getUsername());
                    System.exit(1);
                }
            }
            player.addCard(card);
            player.setNumberOfTurns(player.getNumberOfTurns()-1);
            server.broadcastMessage("DRAWN~" + card,clients.get(getOtherClient()));
            if (player.getNumberOfTurns()>0) {
                server.broadcastMessageWithSender("TURN~" + player.getUsername());
            } else {
                clients.get(getOtherClient()).player.setNumberOfTurns(1);
                server.broadcastMessageWithSender("TURN~" + clients.get(getOtherClient()).player.getUsername());
            }
        } else if (command.startsWith("PLAY_CARD~ATTACK")) {
            player.getPlayerDeck().remove(Card.ATTACK);
            System.out.println("ATTACKED~"+player.getUsername()+"~"+clients.get(getOtherClient()).player.getUsername());
            server.broadcastMessageWithSender("ATTACKED~"+player.getUsername()+"~"+clients.get(getOtherClient()).player.getUsername());
            Player player1 = clients.get(getOtherClient()).player;
            if (player1.canNope()) {
                cardToBeNope = Card.ATTACK;
                player1.setNumberOfTurns(player.getNumberOfTurns()+2);
                server.broadcastMessage("PLAY_NOPE", this);
            } else {
                player1.setNumberOfTurns(player.getNumberOfTurns()+2);
                server.broadcastMessageWithSender("TURN~" + player1.getUsername());
            }
        } else if (command.startsWith("PLAY_CARD~NOPE")) {
            player.getPlayerDeck().remove(Card.NOPE);
            Player player1 = clients.get(getOtherClient()).player;
            if (cardToBeNope != null) {
                if (cardToBeNope.equals(Card.ATTACK)) {
                    player.setNumberOfTurns(0);
                    player1.setNumberOfTurns(1);
                    server.broadcastMessageWithSender("GETS_NOPED~" + player1.getUsername() + "~" + cardToBeNope + "~" + player.getUsername());
                    server.broadcastMessageWithSender("TURN~" + clients.get(getOtherClient()).player.getUsername());
                    cardToBeNope = Card.EXPLODING_KITTEN;
                } else if (cardToBeNope.equals(Card.FAVOR) || cardToBeNope.equals(Card.CAT1)
                        || cardToBeNope.equals(Card.CAT2) || cardToBeNope.equals(Card.CAT3) || cardToBeNope.equals(Card.CAT4)
                        || cardToBeNope.equals(Card.CAT5)) {
                    server.broadcastMessageWithSender("GETS_NOPED~" + player1.getUsername() + "~" + cardToBeNope + "~" + player.getUsername());
                    server.broadcastMessageWithSender("TURN~" + player1.getUsername());
                    cardToBeNope = Card.EXPLODING_KITTEN;
                } else {
                    server.broadcastMessageWithSender("TURN~" + player.getUsername());
                }
            } else {
                server.broadcastMessageWithSender("TURN~" + player.getUsername());
            }
        } else if (command.startsWith("REFUSE_NOPE")) {
            if (cardToBeNope.equals(Card.ATTACK)) {
                server.broadcastMessageWithSender("TURN~" + player.getUsername());
            } else if (cardToBeNope.equals(Card.FAVOR) || cardToBeNope.equals(Card.CAT1)
                    || cardToBeNope.equals(Card.CAT2)  || cardToBeNope.equals(Card.CAT3)  || cardToBeNope.equals(Card.CAT4)
                    || cardToBeNope.equals(Card.CAT5) ) {
                System.out.println("PICK_CARD_IN_HAND~" + player.getUsername());
                sendMessage("PICK_CARD_IN_HAND~" + player.getUsername());
            }
        } else if (command.equalsIgnoreCase("PLAY_CARD~SKIP")) {
            player.setNumberOfTurns(player.getNumberOfTurns()-1);
            player.getPlayerDeck().remove(Card.SKIP);
            if (player.getNumberOfTurns()>0) {
                server.broadcastMessageWithSender("TURN~" + player.getUsername());
            } else {
                server.broadcastMessageWithSender("TURN~"+clients.get(getOtherClient()).player.getUsername());
            }
        } else if (command.equalsIgnoreCase("PLAY_CARD~SHUFFLE")) {
            player.getPlayerDeck().remove(Card.SHUFFLE);
            game.shuffle();
            if (player.getNumberOfTurns()>0) {
                server.broadcastMessageWithSender("TURN~" + player.getUsername());
            } else {
                server.broadcastMessageWithSender("TURN~"+clients.get(getOtherClient()).player.getUsername());
            }
        } else if (command.equalsIgnoreCase("PLAY_CARD~FUTURE")) {
            player.getPlayerDeck().remove(Card.FUTURE);
            if (game.getGameDeck().size()>=3) {
                sendMessage("FUTURE~" + game.getGameDeck().get(0) + "~" + game.getGameDeck().get(1) + "~" + game.getGameDeck().get(2));
            } else if (game.getGameDeck().size()>=2) {
                sendMessage("FUTURE~" + game.getGameDeck().get(0) + "~" + game.getGameDeck().get(1));
            } else if (!game.getGameDeck().isEmpty()) {
                sendMessage("FUTURE~" + game.getGameDeck().get(0));
            }
            server.broadcastMessageWithSender("TURN~"+player.getUsername());
        } else if (command.startsWith("PLAY_FAVOR")) {
            Player player1 = clients.get(getOtherClient()).player;
            cardToBeNope = Card.FAVOR;
            player.getPlayerDeck().remove(Card.FAVOR);
            if (player1.canNope()) {
                server.broadcastMessage("PLAY_NOPE", this);
            } else {
                server.broadcastMessage("PICK_CARD_IN_HAND~" + command.split("~")[1],this);
            }
        } else if (command.startsWith("CHOOSE_CARD_IN_HAND")) {
            Player player1 = clients.get(getOtherClient()).player;
            player.getPlayerDeck().remove(Card.valueOf(command.split("~")[1]));
            player1.addCard(Card.valueOf(command.split("~")[1]));
            sendMessage("CARD_GIVEN~"+cardToBeNope+"~"+player1.getUsername()+"~"+command.split("~")[1]);
            server.broadcastMessage("CARD_RECEIVED~"+cardToBeNope+"~"+player.getUsername()+"~"+command.split("~")[1],this);
            server.broadcastMessage("TURN~"+player1.getUsername(),this);
        } else if (command.startsWith("PLAY_CARD~CAT")) {
            Card card = Card.valueOf(command.split("~")[1]);
            if (player.getNumberOfSameCard(card)>=2) {
                player.getPlayerDeck().remove(card);
                player.getPlayerDeck().remove(card);
                Player player1 = clients.get(getOtherClient()).player;
                cardToBeNope = card;
                if (player1.canNope()) {
                    server.broadcastMessage("PLAY_NOPE", this);
                } else {
                    server.broadcastMessage("PICK_CARD_IN_HAND~" + command.split("~")[1],this);
                }
            } else {
                player.getPlayerDeck().remove(card);
                sendMessage("TURN~"+player.getUsername());
            }
        } else if (command.startsWith("PLAY_DEFUSE")) {
            int index = Integer.parseInt(command.split("~")[1]);
            player.playCard(Card.DEFUSE);
            game.addDefuseCard();
            game.shuffle();
            game.addExplodingKitten(index);
            server.broadcastMessageWithSender("ANNOUNCEMENT~" + player.getUsername() + "~PLACED~" + Card.DEFUSE);
            return "TURN~" + player.getUsername();
        } else if (command.startsWith("REFUSE_DEFUSE")) {
            server.broadcastMessageWithSender("GAME_FINISHED~" + clients.get(getOtherClient()).player.getUsername());
            System.exit(1);
        } else if (command.startsWith("CHAT")) {
            System.out.println(command);
            server.broadcastMessageWithSender("CHAT~" + player.getUsername() + "~" + command.split("~")[1]);
            sendMessage("TURN~"+player.getUsername());
        }
        return "";
    }

    /**
     * @requires client list to be of two clients already connected to the server
     * @requires server to be instanced
     * @return integer that shows where the other client is in the list of clients
     */
    public int getOtherClient () {
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i) != this) {
                return i;
            }
        }
        return -1;
    }


    //for testing purposes, returns socket used in this class to test server
    public Socket getSocket() {
        return this.socket;
    }
}
