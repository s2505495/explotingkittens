package ClientSide.View;

import Model.Card;

import java.util.ArrayList;

public class ClientView {

    /**
     * @requires player to be inside the game and registered as client
     * @ensures printing a message indicating a players turn, including their hand representing the cards they have
     * @param playerDeck representing the current players deck
     */
    public void showTurn(ArrayList<Card> playerDeck){
        System.out.println();
        System.out.println("Its Your Turn");
        System.out.println("Your Hand: ");
        for (int i = 0; i < playerDeck.size(); i++) {
            System.out.println("Card " + (i+1) + ": " + playerDeck.get(i));
        }
    }

    /**
     * @ensures printing of all options the player has as well as instructions to enter choice using an integer
     * @requires turn to match the player in the list of clients
     */
    public void showOptions(){
        System.out.println("Choose One of the options below: ");
        System.out.println("1. Draw Card");
        System.out.println("2. Play Your Card");
        System.out.println("3. Send Chat");
        System.out.print("Enter your choice: ");
    }

    /**
     *
     * @param opponent to take the turn
     * @ensures showing whose turn it is in the console using their username
     * @requires game of two players to be happening
     *
     */
    public void showOtherTurn(String opponent){
        System.out.println("Its " + opponent + " turn");
    }


    /**
     *
     * @param card that is drawn
     * @requires card not null, game started and game deck to be not null
     * @ensures printing a string representation of the drawn card
     */
    public void showDrawn(Card card){
        System.out.println("You Have Drawn " + card);
    }

    /**
     *
     * @requires player to be attacked in the handleCommand method
     * @ensures printing to console that the player has been attacked
     */
    public void showAttack(){
        System.out.println("You have been attacked!");
    }

    /**
     *
     * @param victim that is attacked by player
     * @requires game to have started, attack card to have been played, no nopes played and victim not null
     * @ensures printing a string representation of who was attacked by the player, represented by their username
     */
    public void showAttackVictim(String victim){
        System.out.println("You Attacked " + victim);
    }


    /**
     * @requires player to have a nope card and the other player to have played an action card
     * @ensures printing string representation of choice to play nope card by the player
     */
    public void showNopeOption(){
        System.out.print("Do you want to play your nope card? (Yes/No) : ");
    }


    /**
     *
     * @param victim that is noped
     * @param card that is noped
     * @param noper player that has played the nope card
     * @requires all parameters not null, game started, server started
     * @ensures showing what card from who has been cancelled by a nope played by the player as a string representation
     */
    public void showNopedCard(String victim, String card, String noper){
        System.out.println(victim + "'s " + card + " card was noped by " + noper);
    }


    /**
     *
     * @param data to contain the three future cards in the game deck
     * @requires gamedeck not null, at least have three cards
     * @ensures printing the top 3 cards in the pile left by the deck
     */
    public void showTheFuture(String[] data){
        System.out.print("Top three cards are: ");
        for (int i = 1; i < data.length; i++) {
            System.out.print(data[i] + " ");
        }
        System.out.println();
    }


    /**
     *
     * @param playerDeck that contains the current players deck
     * @requires playerdeck not null, at least have 1 card
     * @ensures querying the user to see which card to give out as a favor
     */
    public void showFavorVictim(ArrayList<Card> playerDeck){
        System.out.println("You have been asked a Favor.");
        System.out.println("Your Hand: ");
        for (int i = 0; i < playerDeck.size(); i++) {
            System.out.println("Card " + (i+1) + ": " + playerDeck.get(i));
        }
        System.out.print("Enter Index of card: ");
    }

    /**
     *
     * @param card that is to be given to the enemy player
     * @param robber player that is stealing the card
     * @param reason card played to take away current card
     * @requires card, robber, reason not null, valid reason (either double cat or favor)
     * @ensures Giving the user a string representation of the card they have given to their enemy and why
     */
    public void showCardGiven(String card, String robber, String reason){
        System.out.println("You gave a "+ card +" card to " + robber + " for the reason of " + reason);
    }

    /**
     *
     * @param card that is to be given to the current player
     * @param robber enemy player
     * @param reason card played to take away current card
     * @requires card, robber, reason not null, valid reason (either double cat or favor)
     * @ensures Giving the user a string representation of the card they took from the enemy and why
     */
    public void showCardReceived(String card, String robber, String reason){
        System.out.println("You received a "+ card +" card from " + robber + " for the reason of " + reason);
    }

    /**
     * @ensures string representation of drawing an exploding kitten
     * @requires drawing exploding kitten card
     */
    public void showExploding(){
        System.out.println("You drew an exploding kitten!");
    }

    /**
     *
     * @param victim that drew an exploding kitten
     * @requires victim not null, victim to have drawn an exploding kitten
     *
     */
    public void showExplodingVictim(String victim){
        System.out.println(victim + " drew an exploding kitten!");
    }


    /**
     *
     * @param winner shown at the end of the game
     * @requires winner not null, game finished by a player exploding
     * @ensures string representation of what user won the game of exploding kittens
     */
    public void showWinner(String winner){

        System.out.println("GAME FINISHED! " + winner + " is the winner!");

    }






}
