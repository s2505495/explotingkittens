package ClientSide.Controller;

import ClientSide.View.ClientView;
import Model.Card;
import Server.ExplodingKittensServer;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.Scanner;

public class ClientProtocolHandler {

    /**
     * @invariant username and opponent, the game is played through two users and both strings are not changed through the duration of the game
     * @invariant view class should be used in handleCommand method and should always be the same object, no need to delete or replace it
     */
    private String username, opponent;

    private ClientView view = new ClientView();
    private ArrayList<Card> playerDeck = new ArrayList<>();

    /**
     *
     * @return proper handshake to server following the communication protocol
     */
    public String sendHandshake () {
        return "HI~COMBOS";
    }

    /**
     *
     * @param command by the current server to be handled by client
     * @requires command to be a non-null string, adhering to the protocol defined by the mentoring group
     * @requires a server class to be running, at least one client running using this class
     * @ensures proper handling of the command sent by the server to client, as well as updating the clients view through the ClientView class
     * @ensures proper updates for the players deck, reacting to the current game
     * @return string indicating current response from the client to the server, adhering to communication protocol
     */

    public String handleCommands (String command) {
//        System.out.println(command);
        Scanner scanner = new Scanner(System.in);
        if (command.equalsIgnoreCase("HI~COMBOS")) {
            return connect();
        } else if (command.equalsIgnoreCase("CONNECTED")) {
            return "REQUEST_GAME~2~0";
        } else if (command.equalsIgnoreCase("GAME_STARTED~COMBOS")) {
            return "REQUEST_CARDS_IN_HAND";
        } else if (command.startsWith("RESPONSE_CARDS_IN_HAND")) {
            String[] cards = command.split("~");
            ArrayList<Card> playerDeck = new ArrayList<>();
            for (int i = 1; i < cards.length; i++) {
                playerDeck.add(Card.valueOf(cards[i]));
            }
            this.playerDeck = playerDeck;
        } else if (command.startsWith("TURN")) {
            if (command.endsWith(username)) {
                scanner = new Scanner(System.in);
                view.showTurn(playerDeck);
                while (true) {
                    view.showOptions();
                    String choice;
                    if(username.equalsIgnoreCase("com")){
                        choice = "1";
                    }
                    else {
                        choice = scanner.nextLine();
                    }
                    if (choice.equalsIgnoreCase("1")) {
                        return "DRAW_CARD";
                    } else if (choice.equalsIgnoreCase("2")) {
                        System.out.print("Enter Index of card: ");
                        String index;
                        if(username.equalsIgnoreCase("com")){
                            index = "2";
                        }
                        else {
                            index = scanner.nextLine();
                        }
                        Card card = playerDeck.remove(Integer.parseInt(index) - 1);
                        if (card.equals(Card.SKIP)) {
                            return "PLAY_CARD~" + card;
                        } else if (card.equals(Card.ATTACK)) {
                            return "PLAY_CARD~" + card;
                        } else if (card.equals(Card.NOPE)) {
                            return "PLAY_CARD~" + card;
                        } else if (card.equals(Card.SHUFFLE)) {
                            return "PLAY_CARD~" + card;
                        } else if (card.equals(Card.FUTURE)) {
                            return "PLAY_CARD~" + card;
                        } else if (card.equals(Card.CAT1) || card.equals(Card.CAT2) || card.equals(Card.CAT3) || card.equals(Card.CAT4) || card.equals(Card.CAT5)) {
                            playerDeck.remove(card);
                            return "PLAY_CARD~" + card;
                        } else if (card.equals(Card.FAVOR)) {
                            return "PLAY_FAVOR~" + opponent;
                        } else if (card.equals(Card.DEFUSE)) {
                            System.out.print("Enter an index to put card (0 - 5) : ");
                            while (true) {
                                String put = scanner.nextLine();
                                try {
                                    if (Integer.parseInt(put) < 5) {
                                        return "PLAY_DEFUSE~" + put;
                                    }
                                } catch (IllegalFormatException ignored) {
                                }
                                System.out.println("Please Enter Valid input");
                            }
                        }
                    } else if (choice.equalsIgnoreCase("3")) {
                        System.out.print("Enter Message: " );
                        String message = scanner.nextLine();
                        return "CHAT~" + message;
                    }
                    System.out.println("InvalidInputException: Please enter valid input.");
                }
            } else {
                opponent = command.split("~")[1];
                view.showOtherTurn(opponent);
            }
        } else if (command.startsWith("DRAWN")) {
            Card card = Card.valueOf(command.split("~")[1]);
            playerDeck.add(card);
            view.showDrawn(card);
            return "REQUEST_CARDS_IN_HAND";
        } else if (command.startsWith("ATTACKED")) {
            String[] data = command.split("~");
            if (data[1].equalsIgnoreCase(opponent)) {
                view.showAttack();
            } else {
                view.showAttackVictim(data[2]);
            }
        } else if (command.startsWith("PLAY_NOPE")) {
            view.showNopeOption();
            String choice;
            if(username.equalsIgnoreCase("com")){
                choice = "no";
            }
            else {
                choice = scanner.nextLine();
            }
            if (choice.equalsIgnoreCase("Yes")) {
                playerDeck.remove(Card.NOPE);
                return "PLAY_CARD~NOPE";
            } else if (choice.equalsIgnoreCase("No")) {
                return "REFUSE_NOPE";
            }
        } else if (command.startsWith("GETS_NOPED")) {
            String[] data = command.split("~");
            view.showNopedCard(data[1], data[2], data[3]);
        } else if (command.startsWith("FUTURE~")) {
            String[] data = command.split("~");
            view.showTheFuture(data);
        } else if (command.startsWith("PICK_CARD_IN_HAND")) {
            view.showFavorVictim(playerDeck);
            String index;
            if(username.equalsIgnoreCase("com")){
                index = "2";
            }
            else {
                index = scanner.nextLine();
            }
            Card card = playerDeck.remove(Integer.parseInt(index)-1);
            return "CHOOSE_CARD_IN_HAND~" + card;
        } else if (command.startsWith("CARD_GIVEN")) {
            String[] data = command.split("~");
            view.showCardGiven(data[3], data[2], data[1]);
        } else if (command.startsWith("CARD_RECEIVED")) {
            String[] data = command.split("~");
            view.showCardReceived(data[3], data[2], data[1]);
            playerDeck.add(Card.valueOf(data[3]));
            System.out.println("Your Hand: ");
            for (int i = 0; i < playerDeck.size(); i++) {
                System.out.println("Card " + (i+1) + ": " + playerDeck.get(i));
            }
        } else if (command.endsWith("DRAWN_EXPLODING_KITTEN")) {
            String[] data = command.split("~");
            if (data[1].equalsIgnoreCase(username)){
                view.showExploding();
            } else {
                view.showExplodingVictim(data[1]);
            }
        } else if (command.startsWith("PLAY_DEFUSE")) {
            System.out.print("Do you want to play your defuse card? (Yes/No) : ");
            String choice;
            if(username.equalsIgnoreCase("com")){
                choice = "no";
            }
            else {
                choice = scanner.nextLine();
            }
            if (choice.equalsIgnoreCase("Yes")) {
                playerDeck.remove(Card.DEFUSE);
                System.out.print("Enter an index to put card (0 ~ " + ExplodingKittensServer.game.getGameDeck().size() + ") : ");
                String put = scanner.nextLine();
                return "PLAY_DEFUSE~" + put;
            } else if (choice.equalsIgnoreCase("No")) {
                return "REFUSE_DEFUSE";
            }
        } else if (command.startsWith("GAME_FINISHED")) {
            view.showWinner(command.split("~")[1]);
            System.exit(0);
        } else if (command.startsWith("CHAT")) {
            String[] data = command.split("~");
            System.out.println(data[1] + ": " + data[2]);
        }
        return "";
    }


    /**
     * @requires user to type their username into console using accepted strings in the JVM
     * @invariant "CONNECT~", the first part of the string always adheres to protocol, followed by the username
     * @return message to connect client to the server with the given name
     */
    public String connect() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please Enter Username: ");
        username = scanner.nextLine();
        return "CONNECT~" + username;
    }
}
