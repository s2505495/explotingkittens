package ClientSide;

import ClientSide.Controller.ClientProtocolHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

public class ExplodingKittensClient {

    private static final String SERVER_IP = "localhost";
    private static final int SERVER_PORT = 12345;
    ClientProtocolHandler clientProtocolHandler = new ClientProtocolHandler();

    public static void main(String[] args) {
        new ExplodingKittensClient().startClient();
    }

    public void startClient() {
        try (Socket socket = new Socket(SERVER_IP, SERVER_PORT);
             BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
             Scanner scanner = new Scanner(System.in)) {

            writer.println(clientProtocolHandler.sendHandshake());

            AtomicReference<String> lastCommand = new AtomicReference<>();
            // Start a thread to handle incoming messages
            new Thread(() -> {
                try {
                    String message;
                    while ((message = reader.readLine()) != null) {
                        String command = clientProtocolHandler.handleCommands(message);
                        lastCommand.set(command);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            // Send messages to the server
            do {
                if (!Objects.equals(lastCommand.get(), "") && !Objects.equals(lastCommand.get(), null)) {
                    writer.println(lastCommand.get());
                    lastCommand.set("");
                }

            } while (!"exit".equalsIgnoreCase(lastCommand.get()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
