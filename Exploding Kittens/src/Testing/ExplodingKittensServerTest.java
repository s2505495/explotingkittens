package Testing;

import Server.ExplodingKittensServer;
import Server.Controller.ClientHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.Socket;

import static org.mockito.Mockito.*;

public class ExplodingKittensServerTest {

    @Test
    void testBroadcastMessageWithSender() {
        ClientHandler sender = mock(ClientHandler.class);
        ClientHandler client1 = mock(ClientHandler.class);
        ClientHandler client2 = mock(ClientHandler.class);

        Socket senderSocket = mock(Socket.class);
        Socket client1Socket = mock(Socket.class);
        Socket client2Socket = mock(Socket.class);

        when(sender.getSocket()).thenReturn(senderSocket);
        when(client1.getSocket()).thenReturn(client1Socket);
        when(client2.getSocket()).thenReturn(client2Socket);

        ExplodingKittensServer server = new ExplodingKittensServer();
        ExplodingKittensServer spyServer = spy(server);

        ExplodingKittensServer.clients.add(sender);
        ExplodingKittensServer.clients.add(client1);
        ExplodingKittensServer.clients.add(client2);



        // Call the method to test
        spyServer.broadcastMessageWithSender("Test message");

        // Verify that the expected method calls were made on the clients
        verify(client1).sendMessage(eq("Test message"));
        verify(client2).sendMessage(eq("Test message"));


    }


    @Test
    void testBroadcastMessage() {
        ClientHandler client1 = mock(ClientHandler.class);
        ClientHandler client2 = mock(ClientHandler.class);

        Socket client1Socket = mock(Socket.class);
        Socket client2Socket = mock(Socket.class);

        when(client1.getSocket()).thenReturn(client1Socket);
        when(client2.getSocket()).thenReturn(client2Socket);

        ExplodingKittensServer server = new ExplodingKittensServer();
        ExplodingKittensServer spyServer = spy(server);

        ExplodingKittensServer.clients.add(client1);
        ExplodingKittensServer.clients.add(client2);

        // Call the method to test
        spyServer.broadcastMessage("Test message", client1);

        // Verify that the expected method calls were made on the clients
        verify(client2).sendMessage(eq("Test message"));
    }

    @Test
    void testRemoveClient() {
        ClientHandler client1 = mock(ClientHandler.class);
        ClientHandler client2 = mock(ClientHandler.class);

        ExplodingKittensServer server = new ExplodingKittensServer();
        ExplodingKittensServer spyServer = spy(server);

        ExplodingKittensServer.clients.add(client1);
        ExplodingKittensServer.clients.add(client2);

        // Call the method to test
        spyServer.removeClient(client1);

        // Verify that the client1 was removed
        Assertions.assertFalse(ExplodingKittensServer.clients.contains(client1));


    }



}
