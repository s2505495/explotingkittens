package Testing;

import Model.Card;
import Model.Player;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {

    @Test
    void testConstructorAndGetters() {
        String username = "testUser";
        Player player = new Player(username);

        assertEquals(username, player.getUsername());
        assertEquals(0, player.getNumberOfTurns());
        assertTrue(player.getPlayerDeck().isEmpty());
    }

    @Test
    void testSetters() {
        Player player = new Player("testUser");

        player.setUsername("newUsername");
        assertEquals("newUsername", player.getUsername());

        player.setNumberOfTurns(5);
        assertEquals(5, player.getNumberOfTurns());

        ArrayList<Card> newDeck = new ArrayList<>();
        newDeck.add(Card.EXPLODING_KITTEN);
        player.setPlayerDeck(newDeck);
        assertEquals(newDeck, player.getPlayerDeck());
    }

    @Test
    void testPlayCardByIndex() {
        Player player = new Player("testUser");
        player.addCard(Card.EXPLODING_KITTEN);

        assertEquals(Card.EXPLODING_KITTEN, player.playCard(0));
        assertTrue(player.getPlayerDeck().isEmpty());
    }

    @Test
    void testPlayCardByCard() {
        Player player = new Player("testUser");
        Card explodingKitten = Card.DEFUSE;
        player.addCard(explodingKitten);

        assertTrue(player.playCard(explodingKitten));
        assertTrue(player.getPlayerDeck().isEmpty());
    }

    @Test
    void testAddCard() {
        Player player = new Player("testUser");
        Card card = Card.DEFUSE;

        player.addCard(card);
        assertEquals(1, player.getPlayerDeck().size());
        assertEquals(card, player.getPlayerDeck().get(0));
    }

    @Test
    void testDisplayDeck() {
        Player player = new Player("testUser");
        player.addCard(Card.DEFUSE);
        player.addCard(Card.SKIP);

        assertEquals("DEFUSE~SKIP", player.displayDeck());
    }

    @Test
    void testGetNumberOfSameCard() {
        Player player = new Player("testUser");
        Card card = Card.EXPLODING_KITTEN;
        player.addCard(card);
        player.addCard(card);
        player.addCard(Card.SKIP);

        assertEquals(2, player.getNumberOfSameCard(card));
        assertEquals(0, player.getNumberOfSameCard(Card.NOPE));
    }

    @Test
    void testCanNope() {
        Player player = new Player("testUser");
        player.addCard(Card.NOPE);

        assertTrue(player.canNope());
    }
}
