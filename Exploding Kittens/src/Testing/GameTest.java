package Testing;

import Model.Card;
import Server.Controller.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {


    Game testGame;
    @BeforeEach
    void setUp() {

        testGame = new Game();

    }

    @Test
    void shuffle() {

        ArrayList<Card> deepCopy = new ArrayList<>();
        deepCopy.addAll(testGame.getGameDeck());

        testGame.shuffle();

        Assertions.assertTrue(testGame.getGameDeck().containsAll(deepCopy));
        Assertions.assertNotEquals(testGame.getGameDeck(), deepCopy);


    }

    @Test
    void drawCard() {

        Card removedCard = testGame.getGameDeck().get(0);
        testGame.drawCard();

        //Important to test reference and not value
        Assertions.assertNotSame(removedCard, testGame.getGameDeck().get(0));



    }

    @Test
    void addExplodingKitten() {

        ArrayList<Card> testDeck = new ArrayList<>();
        testDeck.addAll(testGame.getGameDeck());
        testGame.addExplodingKitten();

        Assertions.assertNotEquals(testDeck.size(), testGame.getGameDeck().size());

    }

    @Test
    void addExplodingKittenWithIndex() {

        ArrayList<Card> testDeck = new ArrayList<>();
        testDeck.addAll(testGame.getGameDeck());
        testGame.addExplodingKitten(0);
        Assertions.assertNotEquals(testDeck.size(), testGame.getGameDeck().size());
        Assertions.assertNotEquals(testDeck.get(0), testGame.getGameDeck().get(0));

    }

    @Test
    void addDefuseCard() {

        ArrayList<Card> testDeck = new ArrayList<>();
        testDeck.addAll(testGame.getGameDeck());
        testGame.addDefuseCard();
        Assertions.assertNotEquals(testDeck.size(), testGame.getGameDeck().size());

    }

    @Test
    void getPlayerDeck() {

        ArrayList<Card> playerDeckTest = testGame.getPlayerDeck();

        Assertions.assertEquals(Card.DEFUSE, playerDeckTest.get(0));

        Assertions.assertEquals(8, playerDeckTest.size());

    }

    @Test
    void getGameDeck() {

        Assertions.assertEquals(46, testGame.getGameDeck().size());

    }

}